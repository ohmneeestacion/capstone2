const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

// For easy access to users ids and check parameters during postman tests
router.get('/allusers', (request, response) => {
	UserController.getAllUsers(request, response);
});

// Check if email exits
router.post('/check-email', (request, response) => {
	// Controller function goes here
	UserController.checkEmailExists(request.body).then((result) => {
		response.send(result);
	})
;})
	
// Register user
router.post('/register', (request, response) => {
	UserController.registerUser(request.body).then((result) => {
		response.send(result);
	})
})

// Login user
router.post('/login', (request, response) => {
	UserController.loginUser(request, response);
})

// Get user details
router.post('/details', auth.verify, (request, response) => {
	UserController.getProfile(request.body).then((result) => {
		response.send(result);
	})
})

router.post('/addtocart', auth.verify, (request, response) => {
	UserController.addToCart(request, response);
})

// get user cart
router.get('/cart', auth.verify, (request, response) => {
	UserController.getPurchasedProducts(request, response);
})

// set user as admin (Admin only)
router.put('/:id/modifyrole', auth.verify, auth.verifyAdmin, (request, response) => {
   UserController.modifyRole(request, response);
});

router.get('/orders', auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.getAllOrders(request, response);
})

router.get('/total', auth.verify, (request, response) => {
	UserController.getUserTotal(request, response);
})
module.exports = router;