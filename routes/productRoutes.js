const express = require('express');
const router = express.Router();
const auth = require('../auth.js');
const ProductController = require('../controllers/ProductController.js');
 
// You can destructure the auth variable to extract the function being exported from it. You can then use the functions directly without having to use dot(.) notation.
// const {verify, verifyAdmin} = auth;


// Insert Routes here

// Create single post
router.post('/add', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.addProduct(request, response)
});

// Get all Products for easy access on product ids and parameters during postman tests
router.get('/all', (request, response) => {
	ProductController.getAllProducts(request, response);
});

// Get all active Products
router.get('/active', (request, response) => {
	ProductController.getAllActiveProducts(request, response);
})

// Get a single Product
router.get('/:id', (request,response) => {
	ProductController.getProduct(request, response);
})
// update Product by id
router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.updateProduct(request, response);
})
// archive post
router.put('/:id/inactivate', auth.verify, (request, response) => {
	ProductController.inactivateProduct(request, response);
})

// Activate single Product
router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.activateProduct(request, response);
})

// Search Product by name
router.post('/search', (request, response) => {
	ProductController.searchProducts(request, response);
});


module.exports = router;