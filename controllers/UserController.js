const User = require('../models/User.js');
const Product = require('../models/Product.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports.getAllUsers = (request, response) => {
	return User.find({}).then(result => {
		result.password = "";
		return response.send(result);
	})
}

module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then((result, error) => {
		if(error){
			return {
				message: error.message 
			}
		}

		if (result.length <= 0){
			return false;
		}

		// This will only return true if there are no errors AND there is an existing user from the database.
		return true;
	})
}

module.exports.registerUser = (request_body) => {
	let new_user = new User({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		mobileNo: request_body.mobileNo,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((registered_user, error) => {
		if(error){
			return {
				message: error.message
			};
		}

		return {
			message: 'Successfully registered a user!'
		};
	}).catch(error => console.log(error));
}

module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {
		// Checks if a user is found with an existing email
		if(result == null){
			return response.send({
				message: "The user isn't registered yet."
			}) 
		}

		// If a user was found with an existing email, then check if the password of that user matched the input from the request body
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			// If the password comparison returns true, then respond with the newly generated JWT access token.
			return response.send({
				accessToken: auth.createAccessToken(result),
				userId: result._id
			});
		} else {
			return response.send({
				message: 'Your password is incorrect.'
			})
		}
	}).catch(error => response.send(error));
}

module.exports.getProfile = (request_body) => {
	return User.findOne({_id: request_body.id}).then((user, error) => {
		if(error){
			return {
				message: error.message 
			}
		}

		user.password = "";

		return user;
	}).catch(error => console.log(error));
}

module.exports.addToCart = async (request, response) => {
	// Blocks this function if the user is an admin
	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}

	// [SECTION] Updating User Collection
	// This variable will return true once the user data has been updated
	let isUserUpdated = await User.findById(request.user.id).then(user => {

		let new_order = {
			productId: request.body.productId
		}

		user.orderedProducts.push(new_order);

		return user.save().then(updated_user => true).catch(error => error.message);
	})

	// Sends any error within 'isUserUpdated' as a response
	if(isUserUpdated !== true){
		return response.send({ message: isUserUpdated });
	}

	// [SECTION] Updating product collection
	let isProductUpdated = await Product.findById(request.body.productId).then(product => {
		let new_customer = {
			userId: request.user.id
		}

		product.customers.push(new_customer);

		return product.save().then(updated_product => true).catch(error => error.message)
	})


	if(isProductUpdated !== true){
		return response.send({ message: isProductUpdated });
	}

	// [SECTION] Once isUserUpdated AND isProductUpdated return true
	if(isUserUpdated && isProductUpdated){
		return response.send({ message: 'Add to Cart successfully!' });
	}
}

module.exports.getPurchasedProducts = (request, response) => {
	User.findById(request.user.id)
	.then(result => response.send(result.orderedProducts))
	.catch(error => response.send(error.message))
}

module.exports.modifyRole = (request, response) => {
	let updated_user_details = {
		firstName: request.body.firstName,
		lastName: request.body.lastName,
		email: request.body.email,
		isAdmin: request.body.isAdmin
	};

	return User.findByIdAndUpdate(request.params.id, updated_user_details).then((user, error) => {
		if(error){
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: 'User has been updated successfully!'
		})
	})
}

module.exports.getAllOrders = (request, response) => {
   User.find({})
      .then(users => {
         // Extract orderedProducts from each user and create a single array
         const allOrderedProducts = users.reduce((products, user) => {
            return products.concat(user.orderedProducts);
         }, []);

         return response.json(allOrderedProducts);
      })
	.catch(error => response.send(error.message))
}

