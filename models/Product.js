const mongoose = require('mongoose');

// Schema
const product_schema = new mongoose.Schema({
		name : {
	        type : String,
	        // Requires the data for this our fields/properties to be included when creating a record.
	        // The "true" value defines if the field is required or not and the second element in the array is the message that will be printed out in our terminal when the data is not present
	        required : [true, "Product is required"],
	        unique: true
	    },
	    description : {
	        type : String,
	        required : [true, "Description is required"]
	    },
	    price : {
	        type : Number,
	        required : [true, "Price is required"]
	    },
	    image : {
	    	type : String,
	    },
	    isActive : {
	        type : Boolean,
	        default : true
	    },
	    createdOn : {
	        type : Date,
	        // The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a product is created in our database
	        default : new Date()
	    },
	    // The "enrollees" property/field will be an array of objects containing the user IDs and the date and time that the user purchased to the product
	    // We will be applying the concept of referencing data to establish a relationship between our products and users 
	    customers : [
	        {
	            userId : {
	                type : String,
	                required: [true, "UserId is required"]
	            },
	            purchasedOn : {
	                type : Date,
	                default : new Date() 
	            }
	        }
	    ]
})

module.exports = mongoose.model("Product", product_schema);